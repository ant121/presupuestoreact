import React, {useState} from 'react';
import {v4 as uuidv4} from 'uuid';
import Error from "./Error";
import PropTypes from 'prop-types';

const Formulario = ({guardarGastos, guardarRestante, resetForm}) => {

    //Definir modelos de gastos
    const [newGasto, setNewGasto] = useState({
        id: '',
        type: '',
        cantidad: 0
    });
    //Modelo de error
    const [error, setError] = useState({
        message: '',
        isError: false
    });

    //Funcion que lee el nuevo gasto
    const definirNuevoGasto = (e) => {

        let value;

        if (e.target.name === "cantidad" && isNaN(e.target.value)) {
            value = parseInt(e.target.value);
        } else {
            value = e.target.value;
        }

        setNewGasto({
                ...newGasto,
                [e.target.name]: value
            }
        )
    }

    const {type, cantidad} = newGasto;

    //Submit de Gasto
    const agregarGasto = (e) => {
        e.preventDefault();

        //Validar informacion
        if (
            type.trim() === '' ||
            isNaN(cantidad) ||
            cantidad < 1
        ) {
            setError({
                message: 'Ambos campos son obligatorios o la cantidad es incorrecta',
                isError: true
            });
            return null;
        }

        //si pasa la validacion
        setError({
            message: '',
            isError: false
        });
        newGasto.id = uuidv4();

        if(guardarRestante(cantidad) === 200){
            guardarGastos(
                newGasto
            )
            //Resetear el form
            setNewGasto({
                id: '',
                type: '',
                cantidad: 0
            })
        }else{
            setError({
                message: 'La cantidad ingresada supera el limite del presupuesto',
                isError: true
            });
        }

    }

    return (
        <form
            onSubmit={agregarGasto}
        >
            <h2>Agrega tus gastos aqui</h2>
            {error.isError ? <Error errorMessage={error.message}/> : null}
            <div className="campo">
                <label>Nombre del gasto</label>
                <input
                    type="text"
                    className="u-full-width"
                    name="type"
                    placeholder="Ej. Transporte"
                    value={type}
                    onChange={definirNuevoGasto}
                    required={true}
                />
                <input
                    type="number"
                    className="u-full-width"
                    name="cantidad"
                    placeholder="Ej. 300"
                    value={cantidad}
                    onChange={definirNuevoGasto}
                    required={true}
                />
                <button
                    type="submit"
                    className="button-primary u-full-width"
                >Agregar gasto</button>
                <button
                    type="button"
                    onClick={resetForm}
                    className="button-danger u-full-width"
                >Eliminar datos</button>
            </div>
        </form>
    );
};

Formulario.propTypes = {
    guardarGastos: PropTypes.func.isRequired,
    guardarRestante: PropTypes.func.isRequired,
    resetForm: PropTypes.func.isRequired
}

export default Formulario;
