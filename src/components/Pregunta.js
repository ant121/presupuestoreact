import React, {Fragment, useState} from 'react';
import Error from "./Error";
import PropTypes from 'prop-types';

const Pregunta = ({guardarPresupuesto, guardarRestante}) => {

    // Definir el state de la cantidad
    const [cantidad, setCantidad] = useState(0);
    const [error, setError] = useState(false);

    //Funcion que lee el presupuesto
    const definirPresupuesto = (e) => {
        setCantidad(parseInt(e.target.value));
    }

    // Submit para definir el presupuesto
    const agregarPresupuesto = (e) => {
        e.preventDefault();

        //Validar
        if(cantidad < 1 || isNaN(cantidad)){
            setError(true);
            return null;
        }

        //Si pasa la validacion
        setError(false);
        guardarPresupuesto(cantidad);
        guardarRestante(cantidad);

    }

    return (
        <Fragment>
            <h2>Coloca tu presupuesto</h2>

            { error ? <Error errorMessage="El presupuesto es incorrecto." /> : null}

            <form
                onSubmit={agregarPresupuesto}
            >
                <input
                    type="number"
                    className="u-full-width"
                    placeholder="Coloca tu presupuesto"
                    onChange={definirPresupuesto}
                />
                <button
                    type="submit"
                    className="button-primary u-full-width"
                >Definir presupuesto</button>
            </form>
        </Fragment>
    );


};

Pregunta.propTypes = {
    guardarPresupuesto: PropTypes.func.isRequired,
    guardarRestante: PropTypes.func.isRequired
}

export default Pregunta;



