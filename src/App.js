import React, {useState} from "react";
import Pregunta from "./components/Pregunta";
import Formulario from "./components/Formulario";
import Listado from "./components/Listado";
import ControlPresupuesto from "./components/ControlPresupuesto";

function App() {

    //definir el modelos state
    const [presupuesto, setPresupuesto] = useState(0);
    const [restante, setRestante] = useState(0);
    const [gastos, setGastos] = useState([]);

    const guardarGasto = (gasto) => {

        setGastos(
            [
                ...gastos,
                gasto
            ]
        );

    }

    const guardarRestante = (cantidadGasto) => {

        if(cantidadGasto <= restante){
            // let res = restante - cantidadGasto;
            setRestante(   restante - cantidadGasto );
            return 200;
        }else{
            return null;
        }


    }

    const resetForm = () =>{
        setPresupuesto(0);
        setRestante(0);
        setGastos([]);
    }

    return (
        <div className="container">
            <header>
                <h1>Gasto Semanal</h1>
                <div className="contenido-principal contenido">
                    {presupuesto === 0 ?
                        <Pregunta
                            guardarPresupuesto={setPresupuesto}
                            guardarRestante={setRestante}
                        />
                        :
                        <div className="row">
                            <div className="one-half column">
                                <Formulario
                                    guardarGastos={guardarGasto}
                                    guardarRestante={guardarRestante}
                                    resetForm={resetForm}
                                />
                            </div>
                            <div className="one-half column">
                                <Listado
                                    gastos={gastos}
                                />
                                <ControlPresupuesto
                                    presupuesto={presupuesto}
                                    restante={restante}
                                />
                            </div>
                        </div>
                    }
                </div>

            </header>

        </div>
    );
}

export default App;
